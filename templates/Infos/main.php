<h2><?= $page['incipit'] ?></h2>

<?php

function do__index() {
    ob_start();
    phpinfo();
    $phpinfo = ob_get_contents();
    ob_end_clean();
    $phpinfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $phpinfo);
    echo "<div id='phpinfo'>$phpinfo</div>";
}

function do__extensions() {
    $array1 = get_loaded_extensions();
    $array2 = array("mysqli", "openssl", "fileinfo", "mbstring", "curl", "gd", "intl", "ldap", "exif", "bz2", "sodium", "Zend OPcache", "imagick");
    $array3 = array("curl", "fileinfo", "gd", "json", "mbstring", "mysqli", "session", "zlib", "SimpleXML", "xml", "intl", "cli", "domxml", "ldap", "openssl", "xmlrpc", "APCu");
    $array1Length = count($array1);
    $array2Length = count($array2);
    $array3Length = count($array3);
    sort($array1);
    sort($array2);
    sort($array3);
    $lastElement1 = end($array1);
    $lastElement2 = end($array2);
    $lastElement3 = end($array3);
    echo "<p>{$array1Length} extensions actuellement chargées : ";
    foreach ($array1 as $value) {
        echo "{$value}";
        if($value == $lastElement1) {
        echo ".";
        } else {
        echo ", ";
        }
    }
    echo "</p><p>{$array2Length} extensions persos : ";
    foreach ($array2 as $value) {
        if (in_array( "{$value}" ,$array1 )) {
        echo "<span style=\"color: green;\">{$value}</span>";
        } else {
        echo "<span style=\"color: red;\">{$value}</span>";
        }
        if($value == $lastElement2) {
        echo ".";
        } else {
        echo ", ";
        }
    }
    echo "</p>";
    echo "</p><p>{$array3Length} extensions GLPI nécessaires : ";
    foreach ($array3 as $value) {
        if (in_array( "{$value}" ,$array1 )) {
        echo "<span style=\"color: green;\">{$value}</span>";
        } else {
        echo "<span style=\"color: red;\">{$value}</span>";
        }
        if($value == $lastElement3) {
        echo ".";
        } else {
        echo ", ";
        }
    }
    echo "</p>";
}

// dispatcher
if ($page['action'] == "index") {
    do__index();
} else if ($page['action'] == "extensions") {
    do__extensions();
} else if ($page['action'] == "magick") {
    // TO-DO
}

?>