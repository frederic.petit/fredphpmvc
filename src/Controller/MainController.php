<?php

class Main extends Controller {

    /**
     * Vue main index
     *
     */
    public function index() {

        $page = array(
            'title' => "Main Index",
            'controller' => strtolower(get_class($this)),
            'action' => __FUNCTION__,
            'incipit' => "Welcome to the FredPHPMVC Main Index."
        );
        $this->render('main', compact('page'));
    }

}