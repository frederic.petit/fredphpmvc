<?php

// globals
define('WEBROOT', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));
define('VERSION', file_get_contents(ROOT.'VERSION.txt'));
require_once(ROOT.'config/app.php');

// debug
if (DEBUG === true) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

// Models & Controllers
require_once(ROOT.'src/Model/AppModel.php');
require_once(ROOT.'src/Controller/AppController.php');

// test args
if (isset($_GET['p'])) {
    $params = explode('/', $_GET['p']);
}

// treatment tests
if (str_contains($_SERVER['REQUEST_URI'], '//')) {
    // avoid multiple slashes behavior
    require_once(ROOT.'src/Controller/ErrorsController.php');
    $controller = new Errors();
    $controller->error(10, '');
} else {
    // args treatment
    if (isset($params)) {
        if ($params[0] != "") {
            $controller = ucfirst(htmlentities($params[0]));
            $controllerStr = ucfirst(htmlentities($params[0]));
            $action = isset($params[1]) ? htmlentities($params[1]) : 'index';
            if (file_exists(ROOT.'src/Controller/'.$controller.'Controller.php')) {
                if (include_once(ROOT.'src/Controller/'.$controller.'Controller.php')) {
                    $controller = new $controller();
                    if (method_exists($controller, $action)) {
                        unset($params[0]);
                        unset($params[1]);
                        call_user_func_array([$controller,$action], $params);   
                    } else {
                        // no existing action
                        require_once(ROOT.'src/Controller/ErrorsController.php');
                        $controller = new Errors();
                        $controller->error(1, $action);
                    }
                } else {
                    // no existing controller's file
                    require_once(ROOT.'src/Controller/ErrorsController.php');
                    $controller = new Errors();
                    $controller->error(2, $controllerStr);
                }
            } else {
                // no existing controller
                require_once(ROOT.'src/Controller/ErrorsController.php');
                $controller = new Errors();
                $controller->error(3, $controllerStr);
            }
        } else {
            // empty args, so go index ...
            require_once(ROOT.'src/Controller/MainController.php');
            $controller = new Main();
            $controller->index();
        }
    } else {
        if (($_SERVER['REQUEST_URI'] == $_SERVER['SCRIPT_NAME']) || ($_SERVER['REQUEST_URI']."index.php" == $_SERVER['SCRIPT_NAME'])) {
            // just "index.php" or "/", so go index ...
            require_once(ROOT.'src/Controller/MainController.php');
            $controller = new Main();
            $controller->index();
        } else {
            // no existing controller
            require_once(ROOT.'src/Controller/ErrorsController.php');
            $controller = new Errors();
            $controller->error();
        }
    }
}

?>
