# FredPHPMVC (v1)
| description | status |
| ------ | ------ |
| **Simple & Easy PHP Skeleton in MVC architecture, without active model and routes.**<br /><br />FredPHPMVC (v1) is provided with four Controllers : Main, Datas, Errors & Infos. | <img src="https://gitlab.com/fredericpetit/fredphpmvc1/badges/main/pipeline.svg"/> |

**NOTE** : Used for my personnal usage to provide FredOS Datas in json format. FredOS JSON Datas are not available in this repository.

## FredUnit membership
**This project is a part of FredUnit, see installations recommandations, live-demo, thanks and credit here : https://fredericpetit.fr/unit.html.**
